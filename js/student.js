
const form = document.querySelector('#form')
const provinceTag = document.querySelector('[name="province"]')
const cityTag = document.querySelector('[name="city"]')
const areaTag = document.querySelector('[name="area"]')
const modal = new bootstrap.Modal('#modal')
const toast = new bootstrap.Toast('.my-toast')
let pname, Gid

async function getStuList() {
    const stuList = await axios.get('/students')
    // console.log(stuList);
    document.querySelector('.total').innerHTML = stuList.data.length
    document.querySelector('.list').innerHTML = stuList.data.map(e => {
        return `<tr>
        <td>${e.name}</td>
        <td>${e.age}</td>
        <td>${e.gender === 0 ? '男' : '女'}</td>
        <td>第${e.group}组</td>
        <td>${e.hope_salary}</td>
        <td>${e.salary}</td>
        <td>${e.province + e.city + e.area}</td>
        <td data-id=${e.id}>
        <a href="javascript:;" class="text-success mr-3"><i class="bi bi-pen"></i></a>
        <a href="javascript:;" class="text-danger"><i class="bi bi-trash"></i></a>
        </td>
    </tr> `
    }).join('')
}

async function getProvince() {
    const province = await axios.get('/api/province')
    provinceTag.innerHTML = '<option value="">--省份--</option>' + province.data.map(e => `<option value="${e}">${e}</option>`).join('')
}

async function getCity() {
    const city = await axios.get(`/api/city?pname=${provinceTag.value}`) // 可直接通过标签获取参数，不需要利用事件对象获取
    cityTag.innerHTML = '<option value="">--城市--</option>' + city.data.map(e => `<option value="${e}">${e}</option>`).join('')
}

async function getArea() {
    const area = await axios.get('/api/area', { params: { pname: provinceTag.value, cname: cityTag.value } })
    areaTag.innerHTML = '<option value="">--区域--</option>' + area.data.map(e => `<option value="${e}">${e}</option>`).join('')
}

// 1.查询数据
getStuList()

// 2. 添加数据
document.querySelector('#openModal').addEventListener('click', () => {
    modal.show()
    getProvince()
    Gid = '' // 通过是否携带 id 判断应该触发的事件
    /* 模态框会被多次触发, 不能在此处为元素通过addEventListener注册事件, 否则将重复注册同一个事件, 引发重复提交表单问题 */
})

provinceTag.addEventListener('change', () => {
    areaTag.innerHTML = '<option value="">--区域--</option>'
    getCity()
})

cityTag.addEventListener('change', () => { getArea(); })

// 3. 修改数据
document.querySelector('.list').addEventListener('click', async e => {
    if (e.target.classList.contains('bi-pen')) {
        const id = e.target.parentElement.parentElement.dataset.id
        modal.show()

        // const data = await axios.get('/students/' + id, { params: { Authorization: localStorage.getItem('token') } })
        const data = await axios.get('/students/' + id)
        // console.log(data);
        form.querySelector('[name="name"]').value = data.data.name
        form.querySelector('[name="age"]').value = data.data.age
        form.querySelector('[name="group"]').value = data.data.group
        form.querySelector('[name="hope_salary"]').value = data.data.hope_salary
        form.querySelector('[name="salary"]').value = data.data.salary

        form.querySelectorAll('[name="gender"]')[data.data.gender].checked = true

        await getProvince()
        form.querySelector('[name="province"]').value = data.data.province
        await getCity()
        form.querySelector('[name="city"]').value = data.data.city
        await getArea()
        form.querySelector('[name="area"]').value = data.data.area

        Gid = id // 通过是否携带 id 判断应该触发的事件
    }
})

// 合并点击事件
document.querySelector('#submit').addEventListener('click', async () => {
    const data = serialize(form, { hash: true, empty: true })
    // console.log(data);
    // 非空校验
    if (Object.values(data).some(item => !item)) {
        Toast.fail("数据不能为空, 请检查数据是否填写完整")
        return
    }
    data.age = +data.age
    data.salary = +data.salary
    data.hope_salary = +data.hope_salary
    data.gender = +data.gender
    data.group = +data.group

    try {
        if (!Gid) {
            const res = await axios.post(`/students/`, data)
            Toast.success(`提交学员 ${res.data.name} (id:${res.data.id}) 成功`)
        } else {
            const res = await axios.put(`/students/${Gid}`, data)
            Toast.success(`修改学员 ${res.data.name} (id:${res.data.id}) 成功`)
        }
    } catch (err) {
        Toast.fail(err.data.message)
    }
    modal.hide()
    form.reset()
    getStuList()
})

document.querySelector('.btn-close').addEventListener('click', () => { form.reset() })
document.querySelector('.btn-secondary').addEventListener('click', () => { form.reset() })

// 4. 删除数据
document.querySelector('.list').addEventListener('click', async e => {
    if (e.target.classList.contains('bi-trash')) {
        const id = e.target.parentElement.parentElement.dataset.id
        const name = e.target.parentElement.parentElement.parentElement.firstElementChild.innerHTML
        if (confirm(`您确定要删除学员 ${name} (id:${id}) 吗?`)) {
            try {
                await axios.delete(`/students/${id}`)
                Toast.success(`删除学员 ${name} (id:${id}) 成功`)
            } catch (err) {
                Toast.fail(err.data.message)
            }
            getStuList()
        }
    }
})