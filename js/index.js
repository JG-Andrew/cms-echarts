/* 首页业务分析
1. 页面一加载，ajax请求图表数据
2. 渲染图表数据
3. 退出登录
*/


async function initData() {
    // 请求数据
    // const res = await axios.get('/dashboard', {
    //     // 从本地存储中获取token
    //     headers: { Authorization: localStorage.getItem('token') }
    // })
    const data = await axios.get('/dashboard')
    // console.log(data);

    // 渲染图表
    setOverview(data.data.overview)
    setLine(data.data.year)
    setSalary(data.data.salaryData)
    setLines(data.data.groupData['1'])
    setGender(data.data.salaryData)
    setMap(data.data.provinceData)

    document.querySelector('#btns').addEventListener('click', e => {
        // console.dir(e.target);
        if (e.target.tagName === 'BUTTON') {
            document.querySelector('.btn-blue').classList.remove('btn-blue')
            e.target.classList.add('btn-blue')
            setLines(data.data.groupData[e.target.innerText])
        }
    })
}

// 1. 页面一加载，ajax请求图表数据
initData()

// 2. 渲染图表数据

// 2.0 总体数据
const setOverview = data => {
    document.querySelector('[name="salary"]').innerText = data.salary
    document.querySelector('[name="student_count"]').innerText = data.student_count
    document.querySelector('[name="age"]').innerText = data.age
    document.querySelector('[name="group_count"]').innerText = data.group_count
}
// 2.1 折线图：全国薪资
const setLine = data => {
    const myChart = echarts.init(document.getElementById('line'));
    const xLabel = data.map(e => e.month)
    const yData = data.map(e => e.salary)

    // console.log(data);

    // 指定图表的配置项和数据
    const option = {
        title: {
            text: '学科薪资走势',
            top: 10,
            left: 10,
            textStyle: {
                fontSize: 16,
            },
        },
        tooltip: {

        },
        xAxis: {
            data: xLabel,
            axisLine: {
                lineStyle: {
                    color: '#ccc',
                    type: 'dashed',
                },
            },
            axisLabel: {
                color: '#999'
            }
        },
        yAxis: {
            splitLine: {
                show: true,
                lineStyle: {
                    type: "dashed"
                }
            },
        },
        series: [
            {
                name: '销量',
                type: 'line',
                smooth: 0.5,
                symbol: 'emptyCircle',
                symbolSize: 8,
                lineStyle: {
                    width: 6,
                    // color: '#5c7bd9',
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 1, [
                        {
                            offset: 0,
                            color: '#5470c6'
                        },
                        {
                            offset: 1,
                            color: '#6681ff'
                        }
                    ])
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        {
                            offset: 0,
                            color: 'rgba(138, 193, 244, 1)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    ])
                },
                data: yData,
            }
        ]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
// 2.2 饼图：班级薪资
const setSalary = data => {
    const myChart = echarts.init(document.getElementById('salary'));

    // 指定图表的配置项和数据
    const option = {
        title: {
            text: '班级薪资分布',
            top: 10,
            left: 10,
            textStyle: {
                fontSize: 16,
            },
        },
        tooltip: {
            trigger: 'item'
        },
        legend: {
            bottom: '10%',
            left: 'center'
        },
        color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
        series: [
            {
                name: '班级薪资分布',
                type: 'pie',
                radius: ['50%', '66%'],
                avoidLabelOverlap: false,
                itemStyle: {
                    borderRadius: 16,
                    borderColor: '#fff',
                    borderWidth: 2
                },
                label: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    label: {
                        show: false,
                    }
                },
                labelLine: {
                    show: false
                },
                data: data.map(e => {
                    return { name: e.label, value: e.g_count + e.b_count }
                })
            }
        ]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
// 2.3 柱状图：每组薪资
const setLines = data => {
    const myChart = echarts.init(document.getElementById('lines'));

    // console.log(data);
    xLabel = data.map(e => e.name)
    yData_1 = data.map(e => e.hope_salary)
    yData_2 = data.map(e => e.salary)

    // 指定图表的配置项和数据
    var option = {
        tooltip: {},
        calculable: true,
        grid: {
            top: 60, bottom: 30, left: '7%', right: '7%'
        },
        xAxis: [
            {
                type: 'category',
                // prettier-ignore
                data: xLabel,
                axisLine: {
                    lineStyle: {
                        color: '#ccc',
                        type: 'dashed',
                    },
                },
                axisLabel: {
                    color: '#999'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                splitLine: {
                    show: true,
                    lineStyle: {
                        type: "dashed"
                    }
                },
            }
        ],
        series: [
            {
                name: '期望薪资',
                type: 'bar',
                data: yData_1,
                color: new echarts.graphic.LinearGradient(1, 0, 0, 1, [
                    {
                        offset: 0,
                        color: '#35d39a'
                    },
                    {
                        offset: 1,
                        color: '#d5f6ea'
                    }
                ])
            },
            {
                name: '就业薪资',
                type: 'bar',
                data: yData_2,
                color: new echarts.graphic.LinearGradient(1, 0, 0, 1, [
                    {
                        offset: 0,
                        color: '#4ba0ef'
                    },
                    {
                        offset: 1,
                        color: '#dbefff'
                    }
                ])
            }
        ]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
// 2.4 饼图：男女薪资
const setGender = data => {
    const myChart = echarts.init(document.getElementById('gender'));

    // 指定图表的配置项和数据
    const option = {
        title: [
            {
                text: '男女薪资分布',
                top: 10,
                left: 10,
                textStyle: {
                    fontSize: 16,
                },
            },
            {
                subtext: '男生',
                left: 'center',
                top: '42%',
            },
            {
                subtext: '女生',
                left: 'center',
                top: '82%',
            },
        ],
        tooltip: {
            trigger: 'item'
        },
        color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
        series: [
            {
                name: '男生',
                type: 'pie',
                radius: ['20%', '30%'],
                center: ['50%', '30%'],
                avoidLabelOverlap: false,
                data: data.map(e => {
                    return { name: e.label, value: e.b_count }
                })
            },
            {
                name: '女生',
                type: 'pie',
                radius: ['20%', '30%'],
                center: ['50%', '70%'],
                avoidLabelOverlap: false,
                data: data.map(e => {
                    return { name: e.label, value: e.g_count }
                })
            },
        ]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
// 2.5 地图：学生分布
const setMap = data => {
    const myChart = echarts.init(document.getElementById('map'));

    // 3.2 指定图表的配置项和数据
    const dataList = data
    // console.log(data);

    // 数据设置
    dataList.forEach((item) => {
        // 数据里名字和上面的名字有点不太一样, 需要把多余的文字去掉(替换成空字符串)
        item.name = item.name.replace(/省|回族自治区|吾尔自治区|壮族自治区|特别行政区|自治区/g, '')
    })

    let option = {
        title: {
            text: '籍贯分布',
            top: 10,
            left: 10,
            textStyle: {
                fontSize: 16,
            },
        },
        tooltip: {
            trigger: 'item',
            formatter: params => `${params.name}: ${params.value ? params.value : 0} 位学员`,
            borderColor: 'transparent',
            backgroundColor: 'rgba(0,0,0,0.5)',
            textStyle: {
                color: '#fff',
            },
        },
        visualMap: {
            min: 0,
            max: 6,
            left: 'left',
            bottom: '20',
            text: ['6', '0'],
            inRange: {
                color: ['#ffffff', '#0075F0'],
            },
            show: true,
            left: 40,
        },
        geo: { // 地理坐标系组件
            map: 'china',
            roam: false,
            zoom: 1.0,
            label: {
                normal: {
                    show: true,
                    fontSize: '10',
                    color: 'rgba(0,0,0,0.7)',
                },
            },
            itemStyle: {
                normal: {
                    borderColor: 'rgba(0, 0, 0, 0.2)',
                    color: '#ffffff', // 地图默认色
                },
                emphasis: {
                    areaColor: '#34D39A',
                    shadowOffsetX: 0,
                    shadowOffsetY: 0,
                    shadowBlur: 20,
                    borderWidth: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)',
                },
            },
        },
        series: [
            {
                name: '籍贯分布',
                type: 'map',
                geoIndex: 0,
                data: dataList,
            },
        ],
    }


    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}

// 3. 退出登录
document.querySelector('#logout').addEventListener('click', () => {
    if (confirm('您确定要退出吗？')) {
        localStorage.removeItem('token')
        location.href = './login.html'
    }
})