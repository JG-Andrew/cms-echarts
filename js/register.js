/* 点击注册按钮：注册业务
1. 获取表单值
2. 表单校验，账号2-30位，密码6-30位
3. 发送ajax请求
4. 响应成功：跳转登录页
*/

const form = document.querySelector('form')

document.querySelector('#btn-register').addEventListener('click', async () => {
    // 1. 获取表单值
    const data = serialize(form, { hash: true, empty: true })
    console.log(data.username, data.password);

    // 2. 表单校验，账号2-30位，密码6-30位
    if (data.username.length < 2 || data.username.length > 30) {
        // Toast.toast('请输入2~30位长度的账号', "danger")
        Toast.fail('账号必须是2~30位')
        return
    } else if (data.password.length < 6 || data.password.length > 30) {
        // Toast.toast('请输入6~30位长度的密码', "danger")
        Toast.fail('密码必须是6~30位')
        return
    } else {
        // 3. 发送ajax请求
        try {
            // const res = await axios({ method: 'post', url: '/register', data })
            const res = await axios.post('/register', data)
            // Toast.toast(res.data.message, "success")
            Toast.success(res.message)
            // 4. 响应成功：跳转登录页
            location.href = './login.html'
        } catch (e) {
            // console.dir(e);
            // Toast.toast(e.response.data.message, "danger")
            Toast.fail(e.response.data.message)
        }
    }
})