/* 与网络相关的通用功能 */

// 1. 设置网站基地址
axios.defaults.baseURL = 'http://ajax-api.itheima.net'


// 2. 拦截器

// 2.1 请求拦截器
axios.interceptors.request.use(config => {
    // (1) 如果有token，就在请求头中添加token
    if (localStorage.getItem('token')) {
        config.headers.Authorization = localStorage.getItem('token')
    }
    return config // 发送请求报文，必须有！！！
}, error => {
    return Promise.reject(error) // 返回请求错误
})


// 2.2 响应拦截器
axios.interceptors.response.use(response => {
    // (4) 响应体解构处理
    return response.data // 发送响应报文，必须有！！！
}, error => {
    // (2) 统一错误处理
    Toast.fail(error.response.data.message)

    // (3) 401错误（token失效或没有），则跳转登录页
    if (error.response.status === 401) {
        location.href = './login.html'
    }
    return Promise.reject(error) // 返回响应错误
})
